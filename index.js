// Bài tập 1
const A = "A";
const B = "B";
const C = "C";
var tinhDiem = function () {
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var diemKhuVuc = document.getElementById("txt-khu-vuc").value;
  var diemDoiTuong = document.getElementById("txt-doi-tuong").value;
  var diemMon1 = document.getElementById("txt-diem-1").value * 1;
  var diemMon2 = document.getElementById("txt-diem-2").value * 1;
  var diemMon3 = document.getElementById("txt-diem-3").value * 1;

  if (diemKhuVuc == A) {
    diemKhuVuc = 2;
  } else if (diemKhuVuc == B) {
    diemKhuVuc = 1;
  } else if (diemKhuVuc == C) {
    diemKhuVuc = 0.5;
  } else {
    diemKhuVuc = 0;
  }
  console.log("Điểm khu vực: ", diemKhuVuc);

  if (diemDoiTuong == 1) {
    diemDoiTuong = 2.5;
  } else if (diemDoiTuong == 2) {
    diemDoiTuong = 1.5;
  } else if (diemDoiTuong == 3) {
    diemDoiTuong = 1;
  } else {
    diemDoiTuong = 0;
  }
  console.log("Điểm đối tượng: ", diemDoiTuong);

  var diemTongKet = diemMon1 + diemMon2 + diemMon3 + diemKhuVuc + diemDoiTuong;
  if (
    diemTongKet >= diemChuan &&
    diemMon1 > 0 &&
    diemMon2 > 0 &&
    diemMon3 > 0
  ) {
    document.getElementById(
      "ketQua"
    ).innerHTML = `<div>Chúc mừng bạn đã trúng tuyển. Tổng điểm: ${diemTongKet}</div>`;
  } else {
    document.getElementById(
      "ketQua"
    ).innerHTML = `<div>Bạn đã rớt. Tổng điểm: ${diemTongKet}</div>`;
  }
  console.log("Điểm tổng kết: ", diemTongKet);
};

// Bài tập 2
var tinhTien = function () {
  var hoTen = document.getElementById("txt-ho-ten").value;
  var soKw = document.getElementById("txt-so-kw").value * 1;
  var tongSoTien = null;

  var soTien50KwDau = 500;
  var soTien50KwKe = 650;
  var soTien100KwKe = 850;
  var soTien150KwKe = 1100;
  var soTienKwConLai = 1300;

  if (soKw <= 50) {
    tongSoTien = soKw * soTien50KwDau;
  } else if (soKw <= 100) {
    tongSoTien = soTien50KwDau * 50 + soTien50KwKe * (soKw - 50);
  } else if (soKw <= 200) {
    tongSoTien =
      soTien50KwDau * 50 + soTien50KwKe * 50 + soTien100KwKe * (soKw - 100);
  } else if (soKw <= 350) {
    tongSoTien =
      soTien50KwDau * 50 +
      soTien50KwKe * 50 +
      soTien100KwKe * 100 +
      soTien150KwKe * (soKw - 200);
  } else {
    tongSoTien =
      soTien50KwDau * 50 +
      soTien50KwKe * 50 +
      soTien100KwKe * 100 +
      soTien150KwKe * 150 +
      soTienKwConLai * (soKw - 350);
  }
  document.getElementById(
    "tienDien"
  ).innerHTML = `<div>Họ Tên: ${hoTen}</div><div>Tiền điện: ${tongSoTien}</div>`;
};
